#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
//Piotr Babicz
//procedure shellsort
int main()
{
  clock_t start_t, end_t;
  double total_t;

  int i, j, h, N;                                      //variables
  float x;

  printf("Liczba elementow zbioru\n");                // Liczebnosc zbioru.
  scanf("%i",&N);

  int* A;
  A = (int*) malloc(N * sizeof(int));
  if (A == NULL)                                      //check memory
   {
		printf("ERROR: not enough memory!\n");
		return EXIT_FAILURE;
   }
  printf("Przed sortowaniem:\n\n");
  srand((unsigned)time(NULL));

  for(j = 0; j <= N; j++)
  {
    A[j] = rand() % 100;                              //zapelniam tablice liczbami pseudolosowymi
  }
  for(j = 1; j <= N; j++)
  {
    printf("%3d", A[j]);
  }
  h = pow(2, log2(N)) -1;                             //poczatkowa wartosc h
  start_t = clock();
  printf("\nGoing to scan a big loop, start_t = %ld\n", start_t);
  while(h>=1)                                          //algorytm start
   {
    for(i = h+1; i <= N; i++)
    {
      x = A[i];
      j = i - h;
      while((j > 0) && (x < A[j]))
      {
        A[j + h] = A[j];
        j = j - h;
      }
      A[j + h] = x;
    }
    h /= 2;
  }
  end_t = clock();
  printf("\nEnd of the big loop, end_t = %ld\n", end_t);

  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("\nPo sortowaniu:\n\n");
  for(j = 1; j <= N; j++)
  {
    printf("%3d", A[j]);
  }
  printf("\nTotal time taken by CPU: %f\n", total_t  );

  return 0;
}
